package none.bookmycab.Model;

import java.io.Serializable;

public class DriverLocation extends Response implements Serializable {
    private int ID;
    private int DriverID;
    private Boolean IsLoggedIn;
    private Boolean IsBusy;
    private String CurrentLatitude;
    private String CurrentLongitude;
    private String DeviceFcmID;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getDriverID() {
        return DriverID;
    }

    public void setDriverID(int driverID) {
        DriverID = driverID;
    }

    public Boolean getLoggedIn() {
        return IsLoggedIn;
    }

    public void setLoggedIn(Boolean loggedIn) {
        IsLoggedIn = loggedIn;
    }

    public Boolean getBusy() {
        return IsBusy;
    }

    public void setBusy(Boolean busy) {
        IsBusy = busy;
    }

    public String getCurrentLatitude() {
        return CurrentLatitude;
    }

    public void setCurrentLatitude(String currentLatitude) {
        CurrentLatitude = currentLatitude;
    }

    public String getCurrentLongitude() {
        return CurrentLongitude;
    }

    public void setCurrentLongitude(String currentLongitude) {
        CurrentLongitude = currentLongitude;
    }

    public String getDeviceFcmID() {
        return DeviceFcmID;
    }

    public void setDeviceFcmID(String deviceFcmID) {
        DeviceFcmID = deviceFcmID;
    }
}

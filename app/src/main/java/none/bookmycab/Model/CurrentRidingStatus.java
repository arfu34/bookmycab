package none.bookmycab.Model;

public class CurrentRidingStatus extends Response{
    private int CustomerID;
    private int DriverID;
    private Boolean IsRiding;

    public int getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(int customerID) {
        CustomerID = customerID;
    }

    public int getDriverID() {
        return DriverID;
    }

    public void setDriverID(int driverID) {
        DriverID = driverID;
    }

    public Boolean getRiding() {
        return IsRiding;
    }

    public void setRiding(Boolean riding) {
        IsRiding = riding;
    }
}

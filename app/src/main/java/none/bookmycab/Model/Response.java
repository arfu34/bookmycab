package none.bookmycab.Model;

import java.io.Serializable;

public class Response {
    private int StatusCode;
    private String Message;

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int statusCode) {
        StatusCode = statusCode;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }
}

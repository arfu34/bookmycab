package none.bookmycab.Model;

/**
 * Created by M744182 on 3/15/2017.
 */

public enum RequestEnum {
    HttpGetRequest,
    HttpPostRequest,
    HttpPutRequest,
    HttpDeleteRequest
}

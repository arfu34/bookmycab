package none.bookmycab.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import none.bookmycab.Model.TravelDetails;
import none.bookmycab.R;
import none.bookmycab.Utils.ConstantsUsed;
import none.bookmycab.Utils.ServiceCalls;

public class FareActivity extends Main2Activity {

    TextView fareText = null;
    TravelDetails travelDetails;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fare);

        fareText = (TextView)findViewById(R.id.cost);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            travelDetails = (TravelDetails)extras.getSerializable(ConstantsUsed.TravelDetailsObj);
            fareText.setText("Total cost for the travel: " + travelDetails.getCost() + "$");
        }

        Button okButton = (Button)findViewById(R.id.okButton);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ServiceCalls.getServiceCallsInstance().SetDriverBusyStatus(false).getStatusCode() == 200)
                {
                    openDriverHome();
                }
            }
        });

    }

    private void openDriverHome() {
        Intent intent = new Intent(FareActivity.this, DriverHome.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            moveTaskToBack(true);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}

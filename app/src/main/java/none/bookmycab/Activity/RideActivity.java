package none.bookmycab.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;

import org.w3c.dom.Document;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import none.bookmycab.Utils.GMapV2Direction;
import none.bookmycab.Utils.LocationSearch;
import none.bookmycab.Model.NotificationCall;
import none.bookmycab.Model.RequestEnum;
import none.bookmycab.Model.TravelDetails;
import none.bookmycab.Utils.PerformAsyncGoogleSearch;
import none.bookmycab.R;
import none.bookmycab.Service.ServerRestAPICall;
import none.bookmycab.Utils.ConstantsUsed;
import none.bookmycab.Utils.ServiceCalls;

public class RideActivity extends Main2Activity implements OnMapReadyCallback, LocationListener {
    private EditText searchText = null;
    private Button searchButton = null;
    private Button startStopButton = null;
    private GoogleMap googleMap = null;
    private double customerLat = 0;
    private double customerLong = 0;
    private Marker yourLocationMarker = null;
    private Marker customerMarker = null;
    ArrayList<Marker> searchMarkers = new ArrayList<Marker>();
    private double sourceLat = 0;
    private double sourceLong = 0;
    private double destinationLat = 0;
    private double destinationLong = 0;
    NotificationCall notificationCall;

    private int customerID = 0;
    private Marker selectedSourceMarker = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            notificationCall = (NotificationCall) extras.getSerializable(ConstantsUsed.NotificationCallObject);
            customerID = notificationCall.getCustomerID();
            customerLat = Double.parseDouble(notificationCall.getLatitude());
            customerLong = Double.parseDouble(notificationCall.getLongitude());
        }
        ServiceCalls.getServiceCallsInstance().SetDriverBusyStatus(true);

        searchText = (EditText) findViewById(R.id.searchText);
        searchButton = (Button) findViewById(R.id.ButtonSearch);
        startStopButton = (Button) findViewById(R.id.ButtonStartStop);
        createGps();
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PerformAsyncGoogleSearch searchTask = new PerformAsyncGoogleSearch();
                try {
                    String text = searchText.getText().toString();
                    searchMarkers.clear();
                    ArrayList<LocationSearch> locationSearchArrayList = searchTask.execute(text).get();
                    for (int i = 0; i < locationSearchArrayList.size(); i++) {
                        LocationSearch loc = locationSearchArrayList.get(i);
                        UpdateMap(loc.getLatitude(), loc.getLongitude(), "Search", "Search");
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        });

        startStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (startStopButton.getText().equals("Start Ride")) {
                    if(selectedSourceMarker != null) {
                        updateCustomerRiding();
                        startStopButton.setText("Stop Ride");
                    }
                    else{
                        Toast.makeText(getApplicationContext(), "Please select the destination marker first", Toast.LENGTH_LONG).show();
                    }
                } else {
                    updateCustomerRideCompleted();
                }

            }
        });

        SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.googleMap);
        supportMapFragment.getMapAsync(this);
    }

    private void updateCustomerRideCompleted() {
        getLocation();
        destinationLat = latitude;
        destinationLong = longitude;
        double cost = 5; //base fare 5$
        double kmdistance = distance(sourceLat, sourceLong, destinationLat, destinationLong);
        cost = cost + (kmdistance * 1.5);
        TravelDetails travelDetails = DriverLocationAdd(cost, kmdistance);
        openFareIntent(travelDetails);
    }

    private TravelDetails DriverLocationAdd(double cost, double distanceTravelled){
        TravelDetails driverLocationReq = getInsertObj(cost, distanceTravelled);
        ServerRestAPICall restAPICall = new ServerRestAPICall();
        Gson gson = new Gson();
        String[] params = new String[3];
        params[0] = "http://192.168.0.109/RestAPI/Driver/TravelAdd";
        params[1] = gson.toJson(driverLocationReq);
        params[2] = RequestEnum.HttpPostRequest.toString();
        TravelDetails response = null;
        try {
            String jsonResponse = restAPICall.execute(params).get();
            response = gson.fromJson(jsonResponse, TravelDetails.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    private TravelDetails getInsertObj(double cost, double distanceTravelled) {
        TravelDetails travel = new TravelDetails();
        travel.setDriverID(notificationCall.getDriverID());
        travel.setCustomerID(notificationCall.getDriverID());
        travel.setDestinationLatitude(Double.toString(destinationLat));
        travel.setDestinationLongitude(Double.toString(destinationLong));
        travel.setSourceLatitude(Double.toString(sourceLat));
        travel.setSourceLongitude(Double.toString(sourceLong));
        travel.setDistanceTravelled(distanceTravelled);
        travel.setCost(cost);

        return travel;
    }

    private void openFareIntent(TravelDetails travel){
        Intent intent = new Intent(RideActivity.this, FareActivity.class);
        intent.putExtra(ConstantsUsed.TravelDetailsObj, travel);
        startActivity(intent);
    }

    private double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        return (dist);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    private void updateCustomerRiding() {
        getLocation();
        sourceLat = latitude;
        sourceLong = longitude;

        LatLng source = new LatLng(sourceLat, sourceLong);
        LatLng destination = selectedSourceMarker.getPosition();
        GMapV2Direction md = new GMapV2Direction();
        Document doc = md.getDocument(source, destination,GMapV2Direction.MODE_DRIVING);
        ArrayList<LatLng> directionPoint = md.getDirection(doc);
        PolylineOptions rectLine = new PolylineOptions().width(3).color(Color.RED);

        for (int i = 0; i < directionPoint.size(); i++) {
            rectLine.add(directionPoint.get(i));
        }
        Polyline polylin = googleMap.addPolyline(rectLine);
        ServiceCalls.getServiceCallsInstance().SetCustomerRidingStatus(true, customerID);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        UpdateMap(customerLat, customerLong, "Customer's location", "Customer");
        UpdateMap(latitude, longitude, "Your Location", "Driver");

        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener()
        {
            @Override
            public boolean onMarkerClick(Marker arg0) {
                int index = searchMarkers.indexOf(arg0);
                if(index > -1){
                    selectedSourceMarker = searchMarkers.get(index);
                    selectedSourceMarker.showInfoWindow();
                }
                else{
                    selectedSourceMarker = null;
                }
                return true;
            }
        });

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(customerMarker.getPosition());
        builder.include(yourLocationMarker.getPosition());
        LatLngBounds bounds = builder.build();
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.10);

        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);

        googleMap.animateCamera(cu);
    }

    private void UpdateMap(double latitude, double longitude, String title, String snip) {
        LatLng ltlg = new LatLng(latitude, longitude);
        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(ltlg, 15);
        MarkerOptions marker = new MarkerOptions()
                .position(ltlg)
                .title(title)
                .snippet(snip);
        Marker locationMarker = this.googleMap.addMarker(marker);
        if (title == "Your Location") {
            if (yourLocationMarker != null) {
                yourLocationMarker.remove();
            }
            yourLocationMarker = locationMarker;
            locationMarker.showInfoWindow();
        } else if (title == "Customer's location") {
            customerMarker = locationMarker;
            searchMarkers.add(customerMarker);
        } else {
            searchMarkers.add(locationMarker);
        }
        googleMap.moveCamera(yourLocation);
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            UpdateMap(location.getLatitude(), location.getLongitude(), "Your Location", "Driver");
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            moveTaskToBack(true);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}

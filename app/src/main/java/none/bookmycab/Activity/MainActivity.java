package none.bookmycab.Activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import none.bookmycab.Model.NotificationCall;
import none.bookmycab.R;
import none.bookmycab.Utils.ConstantsUsed;
import none.bookmycab.Utils.ServiceCalls;

public class MainActivity extends Main2Activity {

    private static final String TAG = MainActivity.class.getSimpleName();
    EditText editTextUsername,editTextPassword;
    Button buttonLogin;
    none.bookmycab.Model.Driver driverLoggedIn= null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonLogin = (Button) findViewById(R.id.loginButton);
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTextUsername = (EditText) findViewById(R.id.userIdEditText);
                editTextPassword = (EditText) findViewById(R.id.userPasswordEditText);
                try{
                    driverLoggedIn = ServiceCalls.getServiceCallsInstance().TryLogin(editTextUsername.getText().toString(), editTextPassword.getText().toString());
                    if(driverLoggedIn!= null && driverLoggedIn.getID() > 0) {
                        DriverStaticObj = driverLoggedIn;
                        loginSuccessful();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), "Incorrect username or password", Toast.LENGTH_LONG).show();
                    }
                }
                catch (Exception ex){
                    ex.printStackTrace();
                }
            }
        });

        BroadcastReceiver mRegistrationBroadcastReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(ConstantsUsed.REGISTRATION_COMPLETE)) {
                    deviceFcmId = intent.getStringExtra(ConstantsUsed.FcmId);
                }
            }
        };
        displayFirebaseRegId();
    }

    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(ConstantsUsed.SHARED_PREF, 0);
        String regId = pref.getString(ConstantsUsed.RegID, null);
        deviceFcmId = regId;
        Log.e(TAG, "Firebase reg id: " + regId);
    }

    private void loginSuccessful(){
        Toast toast = Toast.makeText(getApplicationContext(), "Sucesfully logged in using: " + DriverStaticObj.getName() , Toast.LENGTH_LONG);
        toast.show();
        ServiceCalls.getServiceCallsInstance().DeleteDriverLocation();
        Intent intent = new Intent(MainActivity.this, DriverHome.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            moveTaskToBack(true);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}

package none.bookmycab.Activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import none.bookmycab.Model.CurrentRidingStatus;
import none.bookmycab.Model.DriverLocation;
import none.bookmycab.Model.NotificationCall;
import none.bookmycab.R;
import none.bookmycab.Utils.ConstantsUsed;
import none.bookmycab.Utils.ServiceCalls;

public class DriverHome extends Main2Activity implements OnMapReadyCallback {

    private GoogleMap googleMap;
    private DriverLocation driverLocationObj = null;

    private Marker yourLocationMarker = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!isGooglePlayServicesAvailable()) {
            finish();
        }
        setContentView(R.layout.activity_driver_home);

        if(DriverStaticObj == null){
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }

        BroadcastReceiver mRegistrationBroadcastReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(ConstantsUsed.PUSH_NOTIFICATION)) {
                    NotificationCall notificationCall = (NotificationCall) intent.getSerializableExtra(ConstantsUsed.NotificationCallObject);
                    openRideActivityIntent(notificationCall);
                }
            }
        };
        SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.googleMap);
        supportMapFragment.getMapAsync(this);
        createGps();

        CurrentRidingStatus currentRidingStatus = ServiceCalls.getServiceCallsInstance().IsDriverRiding();
        if(currentRidingStatus != null && currentRidingStatus.getRiding()){
            NotificationCall notificationCall = new NotificationCall();
            notificationCall.setLongitude(Double.toString(latitude));
            notificationCall.setLongitude(Double.toString(longitude));
            notificationCall.setCustomerID(currentRidingStatus.getCustomerID());
            openRideActivityIntent(notificationCall);
        }
    }

    private void openRideActivityIntent(NotificationCall notificationCall) {
        Intent intent = new Intent(this, RideActivity.class);
        intent.putExtra(ConstantsUsed.NotificationCallObject, notificationCall);
        startActivity(intent);
    }

    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, this, 0).show();
            return false;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        UpdateMap(latitude, longitude);

    }

    private void UpdateMap(double latitude, double longitude) {
        String carName = "";
        carName = DriverStaticObj.getCar();
        if(driverLocationObj == null || (driverLocationObj != null && driverLocationObj.getID() < 1))
        {
            driverLocationObj = ServiceCalls.getServiceCallsInstance().DriverLocationAdd(deviceFcmId, latitude, longitude);
        }
        LatLng ltlg = new LatLng(latitude, longitude);
        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(ltlg, 15);
        MarkerOptions marker = new MarkerOptions()
                .position(ltlg)
                .title("Your current location")
                .snippet(carName);
        Marker locationMarker = this.googleMap.addMarker(marker);
        if (yourLocationMarker != null) {
            yourLocationMarker.remove();
        }
        yourLocationMarker = locationMarker;
        locationMarker.showInfoWindow();
        googleMap.moveCamera(yourLocation);
    }

    @Override
    protected void locationChanged(Location location){
        UpdateMap(latitude, longitude);
    }
}
package none.bookmycab.Utils;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class PerformAsyncGoogleSearch extends AsyncTask<String, Void, ArrayList<LocationSearch>> {
    @Override
    protected ArrayList<LocationSearch> doInBackground(String... strings) {
        ArrayList<LocationSearch> locationSearchList = new ArrayList<LocationSearch>();
        String searchParam = strings[0];
        searchParam = searchParam.replace(" ", "");
        String url = "https://maps.googleapis.com/maps/api/place/textsearch/json?query="+searchParam+"&sensor=true&key=AIzaSyD4UuOEpLW-xP-bToWpKXipQG04Fi0e4ng";
        try {
            URL urlConn = new URL(url);

            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }
            };

            // Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };

            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

            URLConnection conn = urlConn.openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while((line = reader.readLine()) != null){
                sb.append(line);
            }
            String jsonContent = sb.toString();
            JSONObject jsonResponse;
            jsonResponse = new JSONObject(jsonContent);
            JSONArray jsonMainNode = jsonResponse.optJSONArray("results");
            for(int i=0; i < jsonMainNode.length(); i++) {
                LocationSearch loc = new LocationSearch();
                JSONObject jsonChildNode = jsonMainNode.getJSONObject(i);
                loc.setLocationName(jsonChildNode.optString("formatted_address").toString());
                JSONObject jsonChildResponse = jsonChildNode.getJSONObject("geometry");
                JSONObject jsonChildChildResponse = jsonChildResponse.getJSONObject("location");
                loc.setLatitude(Double.parseDouble(jsonChildChildResponse.optString("lat").toString()));
                loc.setLongitude(Double.parseDouble(jsonChildChildResponse.optString("lng").toString()));
                locationSearchList.add(loc);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return locationSearchList;
    }
}

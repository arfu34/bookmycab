package none.bookmycab.Utils;

import com.google.gson.Gson;

import none.bookmycab.Activity.DriverHome;
import none.bookmycab.Activity.Main2Activity;
import none.bookmycab.Model.CurrentRidingStatus;
import none.bookmycab.Model.Customer;
import none.bookmycab.Model.DriverLocation;
import none.bookmycab.Model.RequestEnum;
import none.bookmycab.Model.Response;
import none.bookmycab.Service.ServerRestAPICall;

public class ServiceCalls {

    public static String httpServer = "http://192.168.0.109/RestAPI/";

    private static ServiceCalls serviceCalls = null;

    public static ServiceCalls getServiceCallsInstance() {
        if(serviceCalls == null){
            serviceCalls = new ServiceCalls();
        }
        return serviceCalls;
    }

    public Response SetDriverBusyStatus(boolean busyStatus){
        ServerRestAPICall restAPICall = new ServerRestAPICall();
        Gson gson = new Gson();
        DriverLocation driverLocation = new DriverLocation();
        driverLocation.setDriverID(Main2Activity.DriverStaticObj.getID());
        driverLocation.setBusy(busyStatus);
        String[] params = new String[3];
        params[0] = httpServer + "Driver/SetDriverBusyStatus";
        params[1] = gson.toJson(driverLocation);
        params[2] = RequestEnum.HttpPostRequest.toString();
        Response response = null;
        try {
            String jsonResponse = restAPICall.execute(params).get();
            response = gson.fromJson(jsonResponse, Response.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public Response SetCustomerRidingStatus(boolean busyStatus, int custId){
        ServerRestAPICall restAPICall = new ServerRestAPICall();
        Gson gson = new Gson();
        CurrentRidingStatus staus = new CurrentRidingStatus();
        staus.setCustomerID(custId);
        staus.setDriverID(Main2Activity.DriverStaticObj.getID());
        staus.setRiding(busyStatus);
        String[] params = new String[3];
        params[0] = httpServer + "Customer/SetCustomerRidingStatus";
        params[1] = gson.toJson(staus);
        params[2] = RequestEnum.HttpPostRequest.toString();
        Response response = null;
        try {
            String jsonResponse = restAPICall.execute(params).get();
            response = gson.fromJson(jsonResponse, Response.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public CurrentRidingStatus IsDriverRiding(){
        ServerRestAPICall restAPICall = new ServerRestAPICall();
        Gson gson = new Gson();
        CurrentRidingStatus customer = new CurrentRidingStatus();
        customer.setCustomerID(Main2Activity.DriverStaticObj.getID());
        String[] params = new String[3];
        params[0] = httpServer + "Customer/CurrentRidingStatus";
        params[1] = gson.toJson(customer);
        params[2] = RequestEnum.HttpGetRequest.toString();
        CurrentRidingStatus response = null;
        try {
            String jsonResponse = restAPICall.execute(params).get();
            if(jsonResponse != null && jsonResponse.trim() != "");
            {
                response = gson.fromJson(jsonResponse, CurrentRidingStatus.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public DriverLocation DriverLocationAdd(String fcmId, double lat, double lng){
        DriverLocation driverLocationReq = getInsertObj(fcmId, lat, lng, true, false, 0);
        ServerRestAPICall restAPICall = new ServerRestAPICall();
        Gson gson = new Gson();
        String[] params = new String[3];
        params[0] = httpServer + "Driver/DriverLocationAdd";
        params[1] = gson.toJson(driverLocationReq);
        params[2] = RequestEnum.HttpPostRequest.toString();
        DriverLocation response = null;
        try {
            String jsonResponse = restAPICall.execute(params).get();
            response = gson.fromJson(jsonResponse, DriverLocation.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public Response DeleteDriverLocation(){
        ServerRestAPICall restAPICall = new ServerRestAPICall();
        Gson gson = new Gson();
        String[] params = new String[3];
        params[0] = httpServer + "Driver/DeleteLocationDetails";
        params[1] = gson.toJson(DriverHome.DriverStaticObj);
        params[2] = RequestEnum.HttpPostRequest.toString();
        Response response = null;
        try {
            String jsonResponse = restAPICall.execute(params).get();
            response = gson.fromJson(jsonResponse, Response.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public none.bookmycab.Model.Driver TryLogin(String userName, String pw){
        ServerRestAPICall restAPICall = new ServerRestAPICall();
        none.bookmycab.Model.Driver driver = new none.bookmycab.Model.Driver();
        driver.setUserName(userName);
        driver.setPassword(pw);
        Gson gson = new Gson();
        String[] params = new String[3];
        params[0] = httpServer + "Driver/TryLogin";
        params[1] = gson.toJson(driver);
        params[2] = RequestEnum.HttpGetRequest.toString();
        none.bookmycab.Model.Driver driverResponse = null;
        try {
            String jsonResponse = restAPICall.execute(params).get();
            driverResponse = gson.fromJson(jsonResponse, none.bookmycab.Model.Driver.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return driverResponse;
    }

    private DriverLocation getInsertObj(String fcmId, double lat, double lng, boolean isLoggedIn, boolean isBusy, int locationId){
        DriverLocation driverLocation = new DriverLocation();
        driverLocation.setDeviceFcmID(fcmId);
        driverLocation.setDriverID(Main2Activity.DriverStaticObj.getID());
        driverLocation.setLoggedIn(isLoggedIn);
        driverLocation.setCurrentLongitude(Double.toString(lat));
        driverLocation.setCurrentLatitude(Double.toString(lng));
        driverLocation.setBusy(isBusy);
        driverLocation.setID(locationId);
        return driverLocation;
    }
}

package none.bookmycab.Utils;

public class ConstantsUsed {
    public static final String SHARED_PREF = "SharedPref";
    public static final String DriverObject = "DriverObj";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";
    public static final String NotificationCallObject = "NotificationCall";
    public static final String FcmId = "FCMDeviceID";
    public static final String TravelDetailsObj = "TravelDetailsObj";
    public static final String RegID = "RegistrationID";
}

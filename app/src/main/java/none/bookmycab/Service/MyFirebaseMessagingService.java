package none.bookmycab.Service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import none.bookmycab.Activity.Main2Activity;
import none.bookmycab.Model.NotificationCall;
import none.bookmycab.Activity.RideActivity;
import none.bookmycab.Utils.ConstantsUsed;
import none.bookmycab.Utils.NotificationUtil;
import none.bookmycab.Utils.ServiceCalls;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        if (remoteMessage == null || Main2Activity.DriverStaticObj == null)
            return;

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());

            try {
                JSONObject json = new JSONObject(remoteMessage.getData().toString());
                handleDataMessage(json);
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }
    }

    private void handleDataMessage(JSONObject json) {
        Log.e(TAG, "push json: " + json.toString());

        try {
            JSONObject data = json.getJSONObject("body");

            Gson gson = new Gson();
            NotificationCall notification = gson.fromJson(data.toString(), NotificationCall.class);

            if (!NotificationUtil.isAppIsInBackground(getApplicationContext())) {
                // app is in foreground, broadcast the push message
                Intent pushNotification = new Intent(ConstantsUsed.PUSH_NOTIFICATION);
                pushNotification.putExtra(ConstantsUsed.NotificationCallObject, notification);
                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

                // play notification sound
                NotificationUtil notificationUtils = new NotificationUtil(getApplicationContext());
                notificationUtils.playNotificationSound();
            } else {
                // app is in background, show the notification in notification tray
                Intent resultIntent = new Intent(getApplicationContext(), RideActivity.class);
                resultIntent.putExtra(ConstantsUsed.NotificationCallObject, notification);

                NotificationCompat.Builder mBuilder =   new NotificationCompat.Builder(this)
                        .setSmallIcon(android.support.v7.appcompat.R.drawable.abc_btn_colored_material) // notification icon
                        .setContentTitle("New Ride!") // title for notification
                        .setContentText(notification.getCustomerName() + " Calling!") // message for notification
                        .setAutoCancel(true); // clear notification after click
                PendingIntent pi = PendingIntent.getActivity(this,0,resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                mBuilder.setContentIntent(pi);
                NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.notify(0, mBuilder.build());
            }
        } catch (JSONException e) {
            Log.e(TAG, "Json Exception: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }
}

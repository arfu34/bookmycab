package none.bookmycab.Service;

import android.os.AsyncTask;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import none.bookmycab.Model.RequestEnum;

public class ServerRestAPICall extends AsyncTask<String[] , Void, String> {
    @Override
    protected String doInBackground(String[]... strings) {
        //requestParams[0] is the URL
        //requestParams[1] is the jSON to send
        String[] requestParams = new String[3];
        requestParams = strings[0];
        String responseJSon = "";
        HttpResponse response = null;
        RequestEnum requestEnum = RequestEnum.HttpGetRequest;
        try{
            requestEnum = RequestEnum.valueOf(requestParams[2]);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }


        HttpParams httpParams = new BasicHttpParams();
        //List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
        //nameValuePairs.add(new BasicNameValuePair("value", strings[1]));
        HttpClient client = new DefaultHttpClient(httpParams);
        String encodedString ="";
        try {
            if(requestParams[1] != null && !requestParams[1].isEmpty()) {
                encodedString  = URLEncoder.encode(requestParams[1], "UTF-8");
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        switch (requestEnum){
            case HttpPutRequest:
                HttpPut putRequest;
                if(encodedString.isEmpty()){
                    putRequest = new HttpPut(requestParams[0]);
                }
                else{
                    putRequest = new HttpPut(requestParams[0] + "?value=" + encodedString);
                }
                putRequest.setHeader("Content-Type", "application/json");
                try {
                    response = client.execute(putRequest);
                }
                catch (Exception ex){
                    ex.printStackTrace();
                }
                break;
            case HttpPostRequest:
                HttpPost postRequest;
                if(encodedString.isEmpty()){
                    postRequest = new HttpPost(requestParams[0]);
                }
                else{
                    postRequest = new HttpPost(requestParams[0] + "?value=" + encodedString);
                }
                postRequest.setHeader("Content-Type", "application/json");
                try {
                    response = client.execute(postRequest);
                }
                catch (Exception ex){
                    ex.printStackTrace();
                }
                break;
            case HttpDeleteRequest:
                HttpDelete deleteRequest;
                if(encodedString.isEmpty()){
                    deleteRequest = new HttpDelete(requestParams[0]);
                }
                else{
                    deleteRequest = new HttpDelete(requestParams[0] + "?value=" + encodedString);
                }
                deleteRequest.setHeader("Content-Type", "application/json");
                try {
                    response = client.execute(deleteRequest);
                }
                catch (Exception ex){
                    ex.printStackTrace();
                }
                break;
            case HttpGetRequest:
            default:
                HttpGet getRequest;
                if(encodedString.isEmpty()){
                    getRequest = new HttpGet(requestParams[0]);
                }
                else{
                    getRequest = new HttpGet(requestParams[0] + "?value=" + encodedString);
                }
                getRequest.setHeader("Content-Type", "application/json");
                try {
                    response = client.execute(getRequest);
                }
                catch (Exception ex){
                    ex.printStackTrace();
                }
                break;
        }
        try {
            HttpEntity resEntity = response.getEntity();
            responseJSon = EntityUtils.toString(resEntity);
            responseJSon = responseJSon.substring(responseJSon.indexOf("{"), responseJSon.lastIndexOf("}") + 1).replaceAll("\\\\","");
            responseJSon = responseJSon.replaceAll("\\\\","");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseJSon;
    }
}
